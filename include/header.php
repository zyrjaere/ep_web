<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="pull-left hidden-xs">Qendra Diagnostike Endokrinologjike - ElitaPlus</p>
          <p class="pull-right"><i class="fa fa-phone" aria-hidden="true"></i>Tel. +377 44 204 240</p>
      </div>
    </div>
  </div>
</div>
    <div id="navigation" data-spy="affix" data-offset-top="20">
    <div class="container">
      <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar" aria-expanded="false"> 
            <span class="icon-bar top-bar"></span> <span class="icon-bar middle-bar"></span> <span class="icon-bar bottom-bar"></span> 
            </button>

             <a class="navbar-brand"  href="index"><img src="images/logo_t.png" alt="logo" style="display: inline;" class="img-responsive1"><span class="elita-logo"> &nbsp;&nbsp;Elita<span class="elita-logo-kuq">Plus</span></span></a> 

             

         </div>
            <div id="fixed-collapse-navbar" class="navbar-collapse collapse navbar-right">
           
             <ul class="nav navbar-nav" id="nav-links">
                 <?php $page = "$_SERVER[REQUEST_URI]"; ?>
                <li <?php if ($page == '/ep_web/index' || $page == '/ep_web/'){    echo "class='active'";}?> ><a id='ae' href="index"> Ballina</a></li>
                <li <?php if ($page == '/ep_web/lajmet'){   echo "class='active'";}?> ><a id='ae' href="lajmet"> Mjekësi</a></li>
                <li <?php if ($page == '/ep_web/mjeket'){   echo "class='active'";}?> ><a id='ae' href="mjeket"> Stafi</a></li>
                <li <?php if ($page == '/ep_web/laboratori'){ echo "class='active'";}?> ><a id='ae' href="laboratori"> Laboratori</a></li>
                <li <?php if ($page == '/ep_web/terminet'){ echo "class='active'";}?> ><a id='ae' href="terminet"> Terminet</a></li>
                <li <?php if ($page == '/ep_web/foto'){     echo "class='active'";}?> ><a id='ae' href="foto"> Foto</a></li>
                <li <?php if ($page == '/ep_web/kontakt'){  echo "class='active'";}?> ><a id='ae' href="kontakt"> Kontakt</a></li>
            </ul>
            </div>
         </nav>
         </div>
       </div>
     </div>
  </div>

