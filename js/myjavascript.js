function contact(){
    
    var f_name = document.getElementById('emri').value;
    var l_name = document.getElementById('mbiemri').value;
    var phone = document.getElementById('tel').value;
    var email = document.getElementById('email').value;
    var message= document.getElementById('mesazhi').value;

    
    
    
    
     //Emri
    if(f_name == null || f_name == '' || f_name ==' ') {
        alert("Emri nuk duhet te jete i zbrazet");
        document.getElementById('emri').focus();
        return false;
        
    }
 
     if(f_name.length <3 || f_name.length > 30) {
        alert("Emri duhet te jete prej 3-30 karaktere");
         document.getElementById('emri').focus();
        return false;
    }
    
    if(!isNaN(f_name)) {
        alert("Emri nuk duhet te permbaj numra");
        document.getElementById('emri').focus();
        return false;
    }
    
    //Mbiemri 
    
    if(l_name == null || l_name == '' || l_name ==' ') {
        alert("Mbiemri nuk duhet te jete i zbrazet");
        document.getElementById('mbiemri').focus();
        return false;
    }
 
     if(l_name.length <3 || l_name.length > 30) {
        alert("Mbiemri duhet te jete prej 3-30 karaktere");
        document.getElementById('mbiemri').focus();
        return false;
    }
    
    if(!isNaN(l_name)) {
        alert("Mbiemri nuk duhet te permbaj numra");
        document.getElementById('mbiemri').focus();
        return false;
    }
    
    
    
    //Telefoni
    if(phone == null || phone == '' || phone ==' ') {
        alert("Telefoni nuk duhet te jete i zbrazet");
        document.getElementById('tel').focus();
        return false;
    }
 
   var count = 0;
     for (var i = 0; i < phone.length; i++){ 
        if (phone.charCodeAt(i) >= 65 || phone.charCodeAt(i) >= 90){
               count +=1;         
            }
         }
    if(count != 0 ){
        alert('Nr.Telefonit nuk duhet te permbaj numra');
        document.getElementById('tel').focus();
        return false;
    }
    
    // Email
    

    if(email == null ||email == '' || email ==' ') {
        alert("Email nuk duhet te jete i zbrazet");
        document.getElementById('email').focus();
        return false;
    }
    //Message
    
    if(message == null ||message == '' || message ==' ') {
        alert("Mesazhi nuk duhet te jete i zbrazet");
        document.getElementById('mesazhi').focus();
        return false;
    }
 
    
}