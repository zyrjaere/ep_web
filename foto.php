<!doctype html>

   <?php include'include/db.php'; ?>

<html lang="en">
<?php require'include/head.php'; ?>


<body>

<!--Loader-->


<!--Top bar-->



<header id="main-navigation">
  <?php
    require'include/header.php';
    ?>
</header>

<!--Page header & Title-->
<section id="page_header">
<div class="page_header_gall">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Foto</h2>
         <div class="page_link"><a href="index">Ballina</a><span><i class="fa fa-long-arrow-right"></i><a href="foto"><font color="red">Foto</font></a></span></div>
      </div>
    </div>
  </div>
    </div>
    </div>
</section>





<section id="gallery" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="work-filter">
          <ul class="text-center">
             <li><a href="javascript:;" data-filter="all" class="active filter">Foto</a></li>
<!--
             <li><a href="javascript:;" data-filter=".technolog" class="filter">Teknologji e perparuar</a></li>


           


             <li><a href="javascript:;" data-filter=".facilities" class="filter">Laboratori </a></li>
             <li><a href="javascript:;" data-filter=".kids" class="filter">Stafi</a></li>
-->
             

          </ul>
        </div>
      </div>
    </div>
      
     
      
     
      
     <div class="row">
      <div class="zerogrid">
        <div class="wrap-container">
          <div class="wrap-content clearfix">
              
               
      <?php
                $query = "SELECT * FROM galeria ";

            $select_galery = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc($select_galery)){

                $id_galeria = $row['id_galeria'];
                $titulli = $row['titulli'];

                $g_id = $row['id_galeria'];
                $title = $row['titulli'];

                $img = $row['path'];
    

            echo' 
              
              
            <div class="col-1-3 mix work-item facilities heading_space">
              <div class="wrap-col">
                <div class="item-container">
                  <div class="image">
                  <div class="imagestatic">
                      
                      
          
                    ';
                $img=str_replace(" ", '%20', $img);
                echo 
                    '  
                    <img src="'.$img.'" alt="work"/>
                    
                      </div>
                      
                    <div class="overlay">
                      <div class="overlay-inner">
                    <a class="fancybox icon" href='.$img.' data-fancybox-group="gallery"><i class=" icon-eye6"></i></a>
                      </div>
                    </div>
                  </div> 
                  <div class="gallery_content">
                    <h3>"'.$titulli.'"</h3>
                   
                
                          
                  </div>
                </div>
              </div>
            </div>    ' ;     }  ?>
      
              
     
          </div>
        </div>
       </div>
      </div>
  </div>
</section>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>




<script src="js/jquery-2.2.3.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.geolocation.edit.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/parallax.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/functions.js" type="text/javascript"></script>


<!--Footer-->
<footer class="padding-top dark">
  <?php
    require'include/footer.php';
    ?>
</footer>



</body>
</html>
