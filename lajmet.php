<!doctype html>
<?php include'include/db.php'; ?>
<html lang="en">
<head>
<?php require'include/head.php'; ?>
</head>

<body>

<!--Loader-->

<!--Top bar-->

<header id="main-navigation">
<?php require'include/header.php';
    ?>
    </header>


<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Lajmet</h2>
         <div class="page_link"><a href="index">Ballina</a><span><i class="fa fa-long-arrow-right"></i><a href="lajmet"><font color="red">Mjeksi</font></a></span></div>
      </div>
    </div>
  </div>
</div>  
</section>




<!-- Blogs -->
<section id="blog" class="padding-top">
  <div class="container">
    <div class="row">
     
      <div class="col-md-8 col-sm-7">
       <?php
                $query = "SELECT  * FROM mjeksia_lajmet order by id_ls desc";

            $select_news = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_news)){
                $ls_id = $row['id_ls'];
                $title = $row['titulli'];
                $pershkrimi=$row['pershkrimi'];
                $img = $row['foto'];
                $date =$row['data']; 
                ?>
         
        <div class="blog_item padding-bottom" id="lajmetscrol">
        <?php
               echo' 
         <a href="lajmet-detail?q_id='.$ls_id.'" class="pershkrimi-lajmet" >   <h2><font size="9"> '.$title.'  </font>  </h2> </a> ' ?> 
           <ul class="comments">
             <li><a><?php echo $date ?></a> </li>
           </ul>
            <?php 
                echo'
          <div class="image_container">
          <div class="image_static">
         <a href="lajmet-detail?q_id='.$ls_id.'" class="pershkrimi-lajmet" > <img src='.$img.' class="img-responsive" alt="blog post"> </a>
          ';?>
          <?php 
               $limit=200;
               
               if(strlen($pershkrimi)<$limit)
               {
          echo '
          <p class="pershkrimi-lajmet"><a href="lajmet-detail?q_id='.$ls_id.'" class="pershkrimi-lajmet" > '.$pershkrimi.' </p> ';
               } 
               else
               {
                  echo substr('<p class="pershkrimi-lajmet">'.$pershkrimi.'</p>',0,$limit);
                   echo '
            <p><'.$pershkrimi.'<span id="meshumehover"><a href="lajmet-detail?q_id='.$ls_id.'" class="pershkrimi-lajmet" >më shumë..</a></span></p>
          ';
               }
            ?> 
          </div> 
        </div>
        </div>  
        <?php } ?>
        
      </div>
      
      <div class="col-md-4 col-sm-5">
       
      <aside class="sidebar">
           <div class="widget">
           
         
            <ul class="tabs">
              <li class="active" rel="tab1">Më të lexuarat</li>
              <li rel="tab2">Të fundit</li>
              <li rel="tab3">Random</li>
            </ul>
            
            
           <div class="tab_container bg_grey">
             
                
          <h3 class="d_active tab_drawer_heading" rel="tab1">Më të lexuarat</h3>
          <div id="tab1" class="tab_content">
           <?php

                $query = "SELECT * FROM mjeksia_lajmet order by views desc LIMIT 4";
                $select_news = mysqli_query($dbc, $query);
               while($row = mysqli_fetch_assoc($select_news)){
               
                $ls_id = $row['id_ls'];
                $title = $row['titulli'];
                $pershkrimi=$row['pershkrimi'];
                $img = $row['foto'];
                $date =$row['data'];
                $views = $row['views'];
                             
                ?>
            <div class="single_comments">
             
<!--              <img alt="" src="images/1234.jpg">-->
              <?php echo '<p><a  href="lajmet-detail?q_id='.$ls_id.'">'.$title.'</a></p> '?>
              <span><?php echo $date ?></span>
            </div><?php }; ?>
            <div class="clearfix"></div>
          </div>            

          
          <h3 class="tab_drawer_heading" rel="tab2">Të fundit</h3>
          <div id="tab2" class="tab_content">
            <?php
//                $query = "SELECT * FROM lajmet_fundit WHERE data > DATE_SUB(curdate(), INTERVAL 1 MONTH) LIMIT 4";
                $query ="SELECT * FROM mjeksia_lajmet order by data desc LIMIT 4";

            $select_news = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_news)){
                $ls_id = $row['id_ls'];
                $title = $row['titulli'];
                $pershkrimi=$row['pershkrimi'];
                $img = $row['foto'];
                $date =$row['data']; 
                ?>
            <div class="single_comments">
<!--              <img alt="" src="images/1234.jpg">-->
              <?php echo '<p><a  href="lajmet-detail?q_id='.$ls_id.'">'.$title.'</a></p> '?>
              <span><?php echo $date ?></span>
            </div><?php }; ?>
          </div>
           <h3 class="tab_drawer_heading" rel="tab3">Random</h3>
           <div id="tab3" class="tab_content">
           <?php
                $query = "SELECT * FROM mjeksia_lajmet LIMIT 4";

            $select_news = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_news)){
                $ls_id = $row['id_ls'];
                $title = $row['titulli'];
                $pershkrimi=$row['pershkrimi'];
                $img = $row['foto'];
                $date =$row['data']; 
                ?>
            <div class="single_comments">
             
<!--              <img alt="" src="images/1234.jpg">-->
              <?php echo '<p><a  href="lajmet-detail?q_id='.$ls_id.'">'.$title.'</a></p> '?>
              <span><?php echo $date ?></span>
            </div><?php }; ?>
            <div class="clearfix"></div>
          </div>       
        </div>
        
          
           </div>
           
           <div class="widget">
             <h3>Lidhjet</h3>
             <ul class="widget_links">
               <li><a href="laboratori">Analizat</a></li>
               <li><a href="kontakt">Kontakto</a></li>
               <li><a href="terminet">Terminet</a></li>

             </ul>
           </div>
           
        </aside>
        
  </div>
</section>



<!--Footer-->
<footer class="padding-top dark">
<?php require'include/footer.php';
    ?>
    </footer>
  
<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>

<script src="js/jquery-2.2.3.js" type="text/javascript"></script>
<script src="js/on_load_ex_script.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.geolocation.edit.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/functions.js" type="text/javascript"></script>
</body>
</html>
