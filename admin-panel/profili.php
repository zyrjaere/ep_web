<?php 
    include '../include/db.php';
if(isset($_SESSION['logged_in']))  {
include 'include/header.php'; 
?>
<div class="content-wrapper">
        <div class="container">
            <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Profili</h1>
                    </div>
                </div>
      
                   
                
            
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Menaxhimi i profilit
                        </div>
                        <div class="panel-body">
                          <?php
  if (isset($_GET["msg"]) && $_GET["msg"] == 'sukses') {
echo "<p class='bg-success' > Perdoruesi u ndryshua me sukses, per te vazhduar ju lutem kyquni  </p>";
      header("refresh:3; url=include/logout.php ");
      
}     elseif(isset($_GET["msg"]) && $_GET["msg"] == 'delete') {
echo "<p class='bg-success' > Perdoruesi u fshi me sukses! </p>";
    
      header("refresh:1; url=profili.php ");
}elseif(isset($_GET["msg"]) && $_GET["msg"] == 'failed') {
echo "<p class='bg-failed' > Nuk perfundoj me sukses - ka ndodhu nje gabim! </p>";
    
      header("refresh:1; url=profili.php ");
}
    
    ?>
                        
                        
         
          
                                  

 
    <form  method="post">
         
  <div class="form-group">
            
    <label for="exampleInputEmail1">Emri</label>
    <input type="text" name="firstname" class="form-control" id="inputEmri" placeholder="Emri" value="<?php echo $_SESSION['firstname']; ?>" />
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Mbiemri</label>
    <input type="text" name="lastname" class="form-control" id="inputMbiemri" placeholder="Mbiemri" value="<?php echo $_SESSION['lastname'] ?>" />
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Perdoruesi</label>
    <input type="text" name="username" class="form-control" id="inputPerdoruesi" placeholder="Perdoruesi"  value="<?php echo  $_SESSION['username'] ?>"/>
  </div>
           <div class="form-group">
                      
    <button onclick="new_pw();return false;" id="btn_new_password" class="btn btn-default" >New Password</button>
    <button onclick="old_pw();return false;" id="btn_old_password" style="display: none" class="btn btn-default" >Old Password</button>
    <div class="form-group" id="new_password" style="display: none">
    <label for="fjalekalimi">Fjalekalimi</label>
    <input type="password" name="password" class="form-control" id="inputFjalekalimi" placeholder="Fjalekalimi" value="" />
  </div>
     </div>
 
  
  
                           
      
<hr>
                          

                           <button class="btn btn-default"
                                   formaction='include/edit_profile.php?staf_id=<?php echo $_SESSION['id_staf']; ?>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;  Ruaj Ndryshimet</button>
                          
                        
                           
</form>
                            </div>
                            </div>

        </div>
    </div>
    <script src="assets/js/new_pw.js"></script>
    <?php require'include/footer.php';  
}else{ header("location: ../index.php");} ?>