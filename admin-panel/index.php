<?php 
    include '../include/db.php';
if(isset($_SESSION['logged_in']))  {
include 'include/header.php'; 
if($_SESSION['roli'] == '1' || $_SESSION['roli'] == '2' ){

?>

<div class="content-wrapper">
    <div class="container">
            
        <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6"> <h1 class="page-head-line" >Terminet - Aktive</h1></div>  
                        
                      <div class="col-md-6"> 
                          <div class="col-md-8 pull-right">
                          <form id="a_ter_shto">
                              <select  class="form-control" placeholder="Ju lutem caktoni Doktorin" id="zgjedh_doktorin" name="zgjedh_doktorin" onchange="getTa();" style="width:200px;" >
                               <?php 
                               $query ="SELECT DISTINCT s.id_staf,sp.akronimi,s.emri,s.mbiemri from staf s inner join staf_pozita sp on sp.id_staf_pozita = s.id_pozita inner join staf_terminet st on s.id_staf = st.id_staf where s.id_pozita = 1 order by (s.emri = \"Erduan\" && s.mbiemri = \"Sefedini\") desc , s.emri asc";
;
                                  
                                $select_doktori_option = mysqli_query($dbc, $query);

                            while($rows = mysqli_fetch_assoc( $select_doktori_option)){
                                   $id_staf=$rows['id_staf'];
                                   $d_pozita = $rows['akronimi'];
                                   $d_emri = $rows['emri'];
                                   $d_mbiemri = $rows['mbiemri'];
                    
                    echo "<option value='$id_staf'>".ucfirst($d_pozita).".".ucfirst($d_emri)." ".ucfirst($d_mbiemri)."</option>";
                         
                            }
                            
                           
                      ?>
                           
                                ?>
                       </select>
                              <button id="a_btn_shto_a" type="button" onclick="window.location.href='terminet.php?sh_ter'" class="btn btn-default pull-right" ><i class="fa fa-plus" aria-hidden="true" ></i>&nbsp; Shto Termin</button>
                          </form>
                          </div>
                          <h1 class="page-head-line" >Terminet - Pasive</h1>
                      </div> 
                   
                       
                     </div>
                </div>
        
        <div class="row">
         
                <div class="col-md-12">
                    
                    
                    
                    <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color:#00FF00;">
                           
                            <p style="display:inline;">Terminet Aktive </p>
                           
                            
                        <div class="col-md-8 pull-right" >
                                   
                               
                         
                            
                            
                            </div>
                            
                            </div>
                            
                            
                           
                        <div class="panel-body">
                            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="min-width:105px;max-width:200px;">Emri Mbiemri</th>
                                <th style="width: 105px;">Nr. Telefonit</th>
                                <th style="width:50px">Ora</th>
                                <th style="width:30px">P</th>
                                <th style="width:180px;"><?php echo date("d-m-Y"); ?></th>
                            </tr>
                        </thead>
                <tbody id="terminet_aktive">
                 
                </tbody>
                
                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="panel panel-default">
                        <div class="panel-heading" style="background-color:#FF0000;">
                           
                            <p style="display:inline;color:#fff">Terminet Pasive </p>
                  
                            </div>
                            
                            
                        <div class="panel-body">
                            <div class="table-responsive">
           <table class="table table-striped table-bordered table-hover">
                        <thead>
                                <tr>
                                    <th style="min-width: 105px;max-width:200px;">Emri Mbiemri</th>
                                    <th style="width: 105px;">Nr. Telefonit</th>
                                    <th style="width:50px">Ora</th>
                                    <th style="width:75px">Data</th>
                                    <th style="width:75px">Konfirmo</th>
                                </tr>
                         </thead>
                                    
                <tbody id="terminet_pasive">
                   
                        
                </tbody>
             
            </table>
                            </div>
                        </div>
                     </div>
                  </div>
                    
                    
                  
                     </div>
                   </div>
                </div>
             </div>



                <script src="assets/js/appointment3.js"></script>
                <script src="assets/js/index_konfirmo_t.js"></script>
                <script src="assets/js/appointment_val.js"></script>
                <script src="assets/js/on_load_ex_script.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
                <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
                <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
                
<?php require'include/footer.php';  
        }else{ echo "<h1>'Nuk keni autorizim per te vazhduar'</h1>";
          header("refresh:3; url=../terminet.php");} 
}else{ header("location: ../index.php");} ?>

    


