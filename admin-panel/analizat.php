<?php 
    include '../include/db.php';
    if(isset($_SESSION['logged_in']))  {
        include 'include/header.php'; 

     if($_SESSION['roli'] == '1'){
?>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
             <div class="row">
                    <div class="col-md-12">
                        <div class="panel-body" id="butonishto"> 
                    <a href="analizat-detail.php" class="btn btn-default"><i class="fa fa-list" aria-hidden="true"></i> &nbsp; Lista e analizave</a>
                    
                     </div>
                        <h1 class="page-head-line">Analizat</h1>
                    </div>
                    
                </div>
                
                  <?php                
                 if(isset($_GET['delete'])){

    // if(isset($_SESSION['user_role'])){

    //     if($_SESSION['user_role'] == 'admin'){

            $analiza_id = mysqli_real_escape_string($dbc, $_GET['delete']);

            $query = "DELETE FROM analizat WHERE id_analiza = {$analiza_id} ";
            $delete_query = mysqli_query($dbc, $query) or die("Query failed! - " . mysqli_error($dbc));
            $reset_ai="ALTER TABLE analizat AUTO_INCREMENT = 1";         
            $reset_query = mysqli_query($dbc, $reset_ai) or die("Query failed! - " . mysqli_error($dbc));
          if($delete_query && $reset_ai )
		{
			//Shfaq nje mesazh qe te dhenat u rujten me sukses dhe ridrejto ne index.php
			
            
            header("location:analizat-detail.php?msg=delete" );
			
			
		}
		else{
			
				header("location:analizat-detail.php?msg=failed" );
			
		  }
        }
		?>
                 

            
                  <div class="row">
                    <div class="col-md-12">
                    
                    </div>
                </div>
                   

                        <div class="panel panel-default">
                        <div class="panel-heading">
                           Laboratori
                        </div>
                        
                        <div class="panel-body">
                        <div class="table-responsive">
                        <?php
    
    if(isset($_POST['create_analiza'])){

        
     
        $analiza_firstname = $_POST['emri'];
        $analiza_kategoria = $_POST['id_kategoria_analizave'];
        $analiza_cmimi1 = $_POST['cmimi1'];
        $analiza_cmimi2 = $_POST['cmimi2'];
        $analiza_pershkrimi = $_POST['pershkrimi']; 
       


        $query = "INSERT INTO analizat(emri, id_kategoria_analizave, cmimi1, cmimi2, pershkrimi) ";
        $query .= "VALUES ('{$analiza_firstname}','{$analiza_kategoria}','{$analiza_cmimi1}','{$analiza_cmimi2}','{$analiza_pershkrimi}')";

        $create_user_query = mysqli_query($dbc, $query) or die("Query failed! - " . mysqli_error($dbc));

        // $the_users_id = mysqli_insert_id($dbc);
         header("refresh:1;");
        echo "<p class='bg-success'>Analzia u shtua me sukses! </p>";   
        
       
     
    }

?>
                        <!--EDIT ANALIZA-->
                            
                            
                            
         <?php if(isset($_GET['edit_lab'])) { 
                            
                 $the_lab_id = mysqli_real_escape_string($dbc, $_GET['edit_lab']);            
                            
                  $query = "SELECT * FROM analizat where id_analiza = {$the_lab_id}";

                    $select_users = mysqli_query($dbc, $query);
                   
                    while($row = mysqli_fetch_assoc($select_users)){

                    $analiza_id = $row['id_analiza'];
                    $kategoria_analizave = $row['id_kategoria_analizave'];
                    $emri = $row['emri'];
                    $cmimi1 = $row['cmimi1'];
                    $cmimi2 = $row['cmimi2'];
                    $pershkrimi = $row['pershkrimi'];      

                            
                            
                            
                            ?>
                                    
                       <form method="post">
             
  <div class="form-group">
    <label for="exampleInputEmail1">Emri</label>
    <input name="emri" type="name" class="form-control" id="name" placeholder="Emri" value="<?php echo $emri; ?>" />
  </div>
  <label for="exampleInputEmail1">Lloji i analizes</label>
   <div>
   
  <select data-toggle="dropdown" class="btn btn-default dropdown-toggle" name="id_kategoria_analizave" id="">
                     <?php
                        $query = "SELECT * FROM kategori_analiza where id_kategoria_analizave = {$kategoria_analizave} ";
            $select_user_role = mysqli_query($dbc, $query);
            while($rows = mysqli_fetch_assoc($select_user_role)){

                $id = $rows['id_kategoria_analizave'];
                $kategoria = $rows['emri'];

            echo'

           

            <option value="'.$id.'">'.$kategoria.'</option>

 '  ?>
     <?php  }; ?>
        </select>
        
       
       
 </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Cmimi 1</label>
    <input name="cmimi1" type="name" class="form-control" id="name" placeholder="Cmimi 1" value="<?php echo $cmimi1; ?>" />
  </div>
  
   <div class="form-group">
    <label for="exampleInputPassword1">Cmimi 2</label>
    <input name="cmimi2" type="name" class="form-control" id="name" placeholder="Cmimi 2" value="<?php echo $cmimi2; ?>"/>
  </div>
  <div class="form-group">
  <label for="exampleInputFile">Pershkrimi</label>
   <input name="pershkrimi" type="text" class="form-control" placeholder="Pershkrimi" value="<?php echo $pershkrimi; ?>"/>
   </div>
                            <hr>
  
                          <?php if(isset($_GET['edit_lab'])) {  
                          ?>
                           <button class="btn btn-default" formaction='include/edit_lab.php?lab_id=<?php echo $the_lab_id; ?>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;  Ruaj Ndryshimet</button>
                       
</form>

                      

                        <?php } else { ?>
                        
                         <?php } ?>
    
    
        <!--/EDIT USER-->
    
					<?php  } }else{ ?>
                                       <form method="post">
             
  <div class="form-group">
    <label for="exampleInputEmail1">Emri</label>
    <input name="emri" type="name" class="form-control" id="name" placeholder="Emri" />
  </div>
  <label for="exampleInputEmail1">Lloji i analizes</label>
   <div>
   
  <select data-toggle="dropdown" class="btn btn-default dropdown-toggle" name="id_kategoria_analizave" id="">
                     <?php
                        $query = "SELECT * FROM kategori_analiza ";
            $select_user_role = mysqli_query($dbc, $query);
            while($rows = mysqli_fetch_assoc($select_user_role)){

                $id = $rows['id_kategoria_analizave'];
                $kategoria = $rows['emri'];

            echo'

           

            <option value="'.$id.'">'.$kategoria.'</option>

 '  ?>
     <?php  }; ?>
        </select>
        
       
       
 </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Cmimi 1</label>
    <input name="cmimi1" type="name" class="form-control" id="cmimi1" placeholder="Cmimi 1" />
  </div>
  
   <div class="form-group">
    <label for="exampleInputPassword1">Cmimi 2</label>
    <input name="cmimi2" type="name" class="form-control" id="cmimi2" placeholder="Cmimi 2" />
  </div>
  <div class="form-group">
  <label for="exampleInputFile">Pershkrimi</label>
   <input id="pershkrimi" name="pershkrimi" type="text" class="form-control" placeholder="Pershkrimi" />
   </div>

                           <hr />
  <button name="create_analiza" type="submit"  onclick="return analizat_val()" class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Shto Analizen</button>
                         
                         <?php } ?>
                          
                          
</form>
    <!-------------------------------------------------------------------->
                        
                       
                           
                            </div>
                            
                </div>
                            
                            </div>
                            
     
                            
                    
 </div>
            
        </div>

<script src="assets/js/analizat_val.js"></script>
   <?php require'include/footer.php' ;}else {  echo "<h1>'Nuk keni autorizim per te vazhduar'</h1>";
        header("refresh:3; url:terminet.php");} }else{ header("location: ../index.php");} ?>
