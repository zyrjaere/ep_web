<?php 
    include '../include/db.php';
    if(isset($_SESSION['logged_in']))  {
        include 'include/header.php'; 

     if($_SESSION['roli'] == '1'){
?>
 <div class="content-wrapper">
        <div class="container">
         <div class="row">
                    <div class="col-md-12">
                    
                        <div class="panel-body" id="butonishto"> <a href="koha.php"  class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Shto Kohen</a>&nbsp;&nbsp;
                    
                    
                     </div>
                        <h1 class="page-head-line">Llidhja e kohes me doktoret</h1>
                    </div>
                      
                </div>
  <form method="post" class="callus">
   
    <div class="col-md-12"> 
       <?php
                         
        if(isset($_POST['create_dok_ter'])){

        $dt_koha = $_POST['koha_termineve'];
        $dt_dok = $_POST['zgjedh_doktorin'];
         if ($dt_koha == 'null' || $dt_dok == 'null'){
             header("refresh:1;");
              echo "<p class='bg-failed'>Gabim! Ju lutem caktoni te dhenat perseri </p>";
         }else{
       
        $query = "INSERT INTO `staf_terminet` (`id_staf`, `id_koha_terminet`) VALUES ('$dt_dok', '$dt_koha')";
        $create_dok_termin = mysqli_query($dbc, $query) or die("Query failed! - " . mysqli_error($dbc));
             
            if($create_dok_termin){                
         header("refresh:1;");
        echo "<p class='bg-success'>Termini  u shtua me sukses! </p>";   
                }else{
                mysqli_error;   
            }
            }
    }
                          
          ?>  
          <?php                
                 if(isset($_GET['delete'])){

            $id_dok_terminet = mysqli_real_escape_string($dbc,$_GET['delete']);
                     

            $query = "DELETE FROM staf_terminet where id_staf_terminet = {$id_dok_terminet} ";
            $delete_query = mysqli_query($dbc, $query);
            
            $query1 = "ALTER TABLE staf_terminet AUTO_INCREMENT = 1";
            $reset_ai_staf_terminet = mysqli_query($dbc, $query1);

            if($delete_query && $reset_ai_staf_terminet)
		{
                        header("refresh:1;location:kohadoktoret.php;");
                         echo "<p class='bg-success'>Termini u largua me sukses</p>";   
	}
		else{
               		 header("refresh:1;location:kohadoktoret.php");
                         echo "<p class='bg-fail'>Termini nuk u largua me sukses</p>";
		}
        }
		?>
    
  <div class="row">
   <div class="col-md-6">
  <label for="exampleInputEmail1">Koha</label><br>
  
   
  <select data-toggle="dropdown" id="koha_termineve" class="btn btn-default dropdown-toggle" name="koha_termineve">
      <option  selected disabled>Ju lutem caktoni Doktorin</option>  
        </select>
        
       
       
 </div>
   <div class="col-md-6">
     
  <label for="llojet_semundjeve">Llojet e semundjeve</label><br>
   
  <select data-toggle="dropdown" class="btn btn-default dropdown-toggle" onchange="getTime(this.value)" name="zgjedh_doktorin" id="zgjedh_doktorin">
      <option value="0">Zgjedh Doktorin</option>
          <?php 
                $query="SELECT s.id_staf,sp.akronimi,s.emri,s.mbiemri from staf s inner join staf_pozita sp on sp.id_staf_pozita = s.id_pozita where s.id_pozita = 1 order by (s.emri = \"Erduan\" && s.mbiemri = \"Sefedini\") desc , s.emri asc";
                $select_staf_terminet=mysqli_query($dbc, $query);
                while($rows = mysqli_fetch_assoc($select_staf_terminet)){
                    
                    $id_staf=$rows['id_staf'];
                    $d_pozita = $rows['akronimi'];
                    $d_emri = $rows['emri'];
                    $d_mbiemri = $rows['mbiemri'];
                    
                    echo "<option value='$id_staf'>".ucfirst($d_pozita).".".ucfirst($d_emri)." ".ucfirst($d_mbiemri)."</option>";
                    
                    ?>
               <?php }; ?>
               
      </select>
       
      </div>
  </div>
                     
                      <hr />
                           
  <button name="create_dok_ter" type="submit"  onclick="return analizat_val()" class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Lidh kohen me doktor</button>
     </div>                         
</form>
<br>
 <div class="row">
     <div class="col-md-12">
                            
 
                        <div class="panel-body">
                                               <?php
  if (isset($_GET["msg"]) && $_GET["msg"] == 'sukses') {
echo "<p class='bg-success' > Lloji i Semundjes  u ndryshua me sukses! </p>";
      header("refresh:1; url=lista-semundjeve.php ");
}     elseif(isset($_GET["msg"]) && $_GET["msg"] == 'delete') {
echo "<p class='bg-success' > Lloji i semundjes u fshi me sukses! </p>";
    
      header("refresh:1; url=lista-semundjeve.php ");
}elseif(isset($_GET["msg"]) && $_GET["msg"] == 'failed') {
echo "<p class='bg-failed' > Nuk perfundoj me sukses - ka ndodhu nje gabim! </p>";
    
      header("refresh:1; url=lista-semundjeve.php ");
}
    
    ?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                    <th>Doktori</th>
                                    <th>Terminet</th>
                                    <th>-</th>
                                    
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
<?php                                         
    $query = "SELECT  st.id_staf_terminet,s.emri,s.mbiemri , kt.koha from staf s inner join staf_terminet st on s.id_staf = st.id_staf inner join koha_termineve kt on kt.id_koha_termineve=st.id_koha_terminet  where s.id_pozita = 1 order by (s.emri = \"Erduan\" && s.mbiemri = \"Sefedini\") desc , s.emri asc ";

    $select_dr_terminet = mysqli_query($dbc, $query);
                   
    while($row = mysqli_fetch_assoc($select_dr_terminet)){

    $id_staf_terminet = $row['id_staf_terminet'];
    $dt_emri = $row['emri'];
    $dt_mbiemri = $row['mbiemri'];
    $dt_koha = $row['koha'];
        
        echo "<tr><td>$dt_emri $dt_mbiemri</td><td>$dt_koha</td><td><a href='kohadoktoret.php?delete=$id_staf_terminet'>Fshij</a></td></tr>";
       ?>
        
   <?php } ?>
  

              

                        
                         
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
     </div>
 </div>

     </div>
     
</div>

<script  src="assets/js/get_time.js"></script>

<?php require'include/footer.php' ;}else {  echo "<h1>'Nuk keni autorizim per te vazhduar'</h1>";
        header("refresh:3; url:terminet.php");} }else{ header("location: ../index.php");} ?>