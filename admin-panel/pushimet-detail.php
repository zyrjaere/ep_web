<?php 
    include '../include/db.php';
if(isset($_SESSION['logged_in']))  {
include 'include/header.php'; 
if($_SESSION['roli'] == '1' || $_SESSION['roli'] == '2' ){

?>

<div class="content-wrapper">
        <div class="container">
            <div id="desc_pushimi">
                
            </div>

            
                   <h1 class="page-head-line">Kërkesat e Pushimeve</h1>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lista e kërkesave për pushime.
                      
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                               <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th >Perdoruesi</th>
                                            <th>Lloji i Kerkeses</th>
                                            <th>Data e Kerkeses</th>
                                            <th>Data e FIllimit</th>
                                            <th>Data e Mbarimit</th>
                                            <th>Kthimi ne Pune</th>
                                            <th>Aryestimi</th>
                                            <th>Statusi</th>
                                            <th>-</th>
                                            <th>-</th>
                                       
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                        <?php                                         
                        $query = "SELECT kp.id_kerkesa,s.id_staf,CONCAT(s.emri,' ',s.mbiemri) s_femri,lk.lloji_kerkeses_akronimi, lk.lloji_kerkeses,kp.data_kerkeses,
                            kp.data_fillimit_pushimit,kp.data_mbarimit_pushimit,kp.data_kthimit_pune,kp.arsyetimi,sk.statusi,kp.pushim_ne_dite FROM kerkesa_pushim kp 
                            inner join staf s on s.id_staf = kp.id_staf inner join statusi_kerkeses sk on sk.id_statusi_kerkeses = kp.id_statusi 
                            inner join lloji_kerkeses lk on lk.id_lloji_kerkeses = kp.id_lloji_kerkeses  order by kp.id_kerkesa desc";

                        $select_kerkesa = mysqli_query($dbc, $query);

                        while($row = mysqli_fetch_assoc($select_kerkesa)){

                        $kerkesat_id = $row['id_kerkesa'];
                        
                        $id_staf = $row['id_staf'];
                        $id_lloji_kerkeses = $row['lloji_kerkeses_akronimi'];
                        $lloji_kerkeses = $row['lloji_kerkeses'];
                        
                        $s_femri = $row['s_femri'];
                       
                        $data_kerkeses = $row['data_kerkeses'];
                        $data_k = date('d-m-Y',strtotime($data_kerkeses));
                            
                        $data_fillimi = $row['data_fillimit_pushimit'];
                        $data_f = date('d-m-Y',strtotime($data_fillimi));
                            
                        $data_mbarimit = $row['data_mbarimit_pushimit'];
                        $data_m = date('d-m-Y',strtotime($data_mbarimit));
                            
                        $kthimi_ne_pune = $row['data_kthimit_pune'];
                        $data_k_np = date('d-m-Y',strtotime($kthimi_ne_pune));
                            
                        $arsyetimi = $row['arsyetimi'];
                        
                        $statusi = $row['statusi'];
                        
                        $ne_dite = $row['pushim_ne_dite'];
                            ?>

                 <?php  
                      
                         
                            echo '<tr>'; 
                            echo '<td >'.$kerkesat_id.'</td>'; 
                            echo '<td ><a style="cursor:pointer" id="'.$kerkesat_id.'" onclick="show_desc_pushimi(this.id)">' .$s_femri.'</a></td>';
                            echo '<td>'.$lloji_kerkeses.'</td>'; 
                            echo '<td>'.$data_k.'</td>';  
                            echo '<td>'.$data_f.'</td>'; 
                            echo '<td>'.$data_m.'</td>'; 
                            echo '<td>'.$data_k_np.'</td>';
                           
                            $limit = 20;
                                    
                            if (strlen($arsyetimi) < $limit)
                            {
                               echo '<td >'.$arsyetimi.'</td>';
                            }
                           else
                           {
//                               echo'<td><a style="cursor:pointer" id="'.$kerkesat_id.'" onclick="show_desc_pushimi(this.id)"></a></td>';
                               echo'<td>' ,substr($arsyetimi,0, $limit);'</td>';
                               echo'<a style="cursor:pointer" id="'.$kerkesat_id.'" onclick="show_desc_pushimi(this.id)"><br>me shume...</a>';
                           };
                                        
                             
                            echo '<td>'.$statusi.'</td>';      
                            echo '<td><a href="include/ch_kerkesa.php?kk_id='.$kerkesat_id.'&id_s='.$id_staf.'&id_lk='.$id_lloji_kerkeses.'&n_d='.$ne_dite.'">Konfirmo </a></td>';
                            echo '<td><a href="include/ch_kerkesa.php?ak_id='.$kerkesat_id.'&id_s='.$id_staf.'">Anulo </a></td>';
                            echo '</tr>'; ?>    
                     
<?php    }; ?>
                       
                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
</div>
</div>

<script src="assets/js/desc_p_popup.js" type="text/javascript"></script>
<script src="assets/js/print_pushimi.js" type="text/javascript"></script>

<?php require'include/footer.php';  
        }else{ echo "<h1>'Nuk keni autorizim per te vazhduar'</h1>";
          header("refresh:3; url=terminet.php");} 
}else{ header("location: ../index.php");} ?>
