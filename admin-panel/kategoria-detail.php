<?php 
include '../include/db.php';
    if(isset($_SESSION['logged_in']))  {
        include 'include/header.php';  
        if($_SESSION['roli'] == '1'){
?>
 <div class="content-wrapper">
        <div class="container">
         <div class="panel-body" id="butonishto"> 
                    <a href="kategoria.php" class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Shto Kategorine </a>
                    
                     </div>
        <h1 class="page-head-line">Kategoria</h1>
 <div class="panel panel-default" >
                        
                        <div class="panel-heading">
                           Lista e kategorive 
                          
                        </div>
                        
                        <div class="panel-body" >
                           <?php    if (isset($_GET["msg"]) && $_GET["msg"] == 'sukses') {
echo "<p class='bg-success' > Kategoria u ndryshua me sukses! </p>";
    
      header("refresh:1; url=kategoria-detail.php ");
}
      elseif(isset($_GET["msg"]) && $_GET["msg"] == 'delete') {
echo "<p class='bg-success' > Kategoria u fshi me sukses! </p>";
    
      header("refresh:1; url=kategoria-detail.php ");
}elseif(isset($_GET["msg"]) && $_GET["msg"] == 'failed') {
echo "<p class='bg-failed' > Nuk perfundoj me sukses - ka ndodhu nje gabim! </p>";
    
      header("refresh:1; url=kategoria-detail.php ");
}
    
    ?>
    
                            <div class="table-responsive" id="print-analiza" >
                                <table class="table table-striped table-bordered table-hover" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Emri i kategoris </th>
                                            <th>-</th>
                                            <th>-</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php                                         
            $query = "SELECT * FROM kategori_analiza";

            $select_kategoria = mysqli_query($dbc, $query);

            while($row = mysqli_fetch_assoc($select_kategoria)){

            $kategoria_id = $row['id_kategoria_analizave'];
            $emri = $row['emri'];
                

                echo '<tr>';
                echo '<td>'.$kategoria_id.'</td>';
                echo '<td>'.$emri.'</td>';
                            

                
                echo "<td><a href='kategoria.php?edit_kat={$kategoria_id}'>Ndrysho</a></td>";
                echo "<td><a onclick='return MyFunction();' href='kategoria.php?delete={$kategoria_id}'> Fshij</a></td>";
                 echo'</tr>';



            }; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
     </div>
</div>
<script src="../js/print.js" type="text/javascript"></script>
<?php require'include/footer.php';  
        }else{ echo "<h1>'Nuk keni autorizim per te vazhduar'</h1>";
        header("refresh:3; url=terminet.php");}}else{ header("location: ../index.php");} ?>