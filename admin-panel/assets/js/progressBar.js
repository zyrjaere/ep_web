function _(el){
    return document.getElementById(el);
}
function uploadFile(){
    
    var pgurl = window.location.href.substr(window.location.href
.lastIndexOf("/")+1);
    
   
if(pgurl == "foto.php"){
    
     var title = document.getElementById('f_titulli').value;
    if(title == '' || title == ' ' || title == null){
        foto_val();return false;
    }
    var file= _("img_gallery").files[0];
    if(!file){
        file = ''; 
    }
    
    var formdata = new FormData();
    formdata.append("img_gallery",file);
    formdata.append("titulli",title);
    var ajax =  new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST","include/add_foto_gallery.php");
    ajax.send(formdata);
}
if(pgurl == "rregullorja.php" || pgurl == "rregullorja.php?msg=delete" ){
      
     var title = document.getElementById('rr_titulli').value;
    
    var file= _("uploaded").files[0];
       
    var formdata = new FormData();
    formdata.append("uploaded",file);
    formdata.append("titulli",title);
    var ajax =  new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST","include/add_rregullorja.php");
    ajax.send(formdata);
}

    
if(pgurl == "doktoret.php"){
    
     var name = document.getElementById('name').value;
    if(name == '' || name == ' ' || name == null){
       alert("Emri nuk duhet te jene i zbrazet");
        document.getElementById('name').focus();
            return false;
    }
    var surname = document.getElementById('surname').value;
    if(surname == '' || surname == ' ' || surname == null){
        alert("Mbiemri nuk duhet te jene i zbrazet");
        document.getElementById('surname').focus();
            return false;
    }
    var dataLindjes = document.getElementById('data_lindjes').value;
    if(dataLindjes == '' || dataLindjes == ' ' || dataLindjes == null){
        alert("Data Lindjes nuk duhet te jene i zbrazet");
        document.getElementById('data_lindjes').focus();
            return false;
    }
    var pozita = document.getElementById('id_staf_pozita').value;
    var profesioni = document.getElementById('profesioni').value;
    if(profesioni == '' || profesioni == ' ' || profesioni == null){
       alert("Profesioni nuk duhet te jene i zbrazet");
        document.getElementById('profesioni').focus();
        return false;
    }
    var pershkrimi = document.getElementById('pershkrimi').value;
    if(pershkrimi == '' || pershkrimi == ' ' || pershkrimi == null){
        alert("Pershkrimi nuk duhet te jene i zbrazet");
        document.getElementById('pershkrimi').focus();
        return false;
    }
    var getDisplay =document.getElementById('perdoruesi').style.display;
    
    if(getDisplay == 'block'){
    
    var username = document.getElementById('username').value;{
        if((username == '' || username == ' ' || username == null))
           //alert("Username nuk duhet te jene i zbrazet");
           // document.getElementById('username').focus();
                return false;
    }
    var fjalkalimi=document.getElementById('inputFjalekalimi').value;{
        if((fjalkalimi == '' || fjalkalimi == ' ' || fjalkalimi == null))
            //alert("Password nuk duhet te jene i zbrazet");
           // document.getElementById('inputFjalekalimi').focus();
              return false;
    }
    var roli = document.getElementById('roli').value;
    
    }
    
    var file = _("img_staf").files[0];
    if(!file){
        file = ''; 
    }

    
    var formdata = new FormData();
    formdata.append("emri",name);
    formdata.append("mbiemri",surname);
    formdata.append("data_lindjes",dataLindjes);
    formdata.append("id_staf_pozita",pozita);
    formdata.append("profesioni",profesioni);
    formdata.append("pershkrimi",pershkrimi);
    formdata.append("img_staf",file);
    if(getDisplay == 'block'){
    formdata.append("username",username);
    formdata.append("password",fjalkalimi);
    formdata.append("roli",roli);
    }
    var ajax =  new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST","include/create_stafi.php");
    ajax.send(formdata); 
}    

 if(pgurl == "lajmet.php"){
    
     var title = document.getElementById('titulli').value;
    if(title == '' || title == ' ' || title == null){
        lajmet_val();return false;
    }
     var pershkrimi = document.getElementById('pershkrimi').value;
    if(pershkrimi == '' || pershkrimi == ' ' || pershkrimi == null){
        lajmet_val();return false;
    }
     
    var file= _("img_lajmet").files[0];
    if(!file){
        file = ''; 
    }
    
    var formdata = new FormData();
    formdata.append("titulli",title);
    formdata.append("pershkrimi",pershkrimi);
    formdata.append("img_lajmet",file);
    var ajax =  new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST","include/create_lajmi.php");
    ajax.send(formdata);
}
function progressHandler(event){
    var percent = (event.loaded / event.total) * 100;
    _("progressBar").value = Math.round(percent);
//    _("status").innerHTML = Math.round(percent) + " % ... Ju lutem prisni! ";
}
function completeHandler(event){
    _("status").innerHTML =  event.target.responseText;
    _("progressBar").value = 0;
    
}
function errorHandler(event){
    _("status").innerHTML = "Ngarkimi Deshtoi ";
    
}
function abortHandler(event){
    _("status").innerHTML = "Ngarkimi Deshtoi ";
    
}
    
    
}