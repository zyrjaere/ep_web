function analizat_val(){
    
    var v_firstname = document.getElementById('name').value;
    var v_cmimi1 = document.getElementById('cmimi1').value;
    var v_cmimi2 = document.getElementById('cmimi2').value;
    var v_pershkrimi = document.getElementById('pershkrimi').value;
    
    
    
     //Emri
    if(v_firstname == null || v_firstname == '' || v_firstname ==' ') {
        alert("Emri nuk duhet te jene i zbrazet");
        document.getElementById('name').focus();
        return false;
        
    }
    
     if(v_firstname.length <2 ) {
        alert("Emri duhet te kete me shume se 3 karaktere");
         document.getElementById('name').focus();
        return false;
    }
    
    if(!isNaN(v_firstname)) {
        alert("Emri nuk duhet te permbaj numra");
        document.getElementById('name').focus();
        return false;
    }
    //cmimi1
     if(v_cmimi1 == null || v_cmimi1 == '' || v_cmimi1 ==' ') {
        alert("Cmimi-1 nuk duhet te jete i zbrazet");
        document.getElementById('cmimi1').focus();
        return false;
        
    }
    
    //cmimi1
     if(isNaN(v_cmimi1)) {
        alert("Cmimi-1 nuk duhet te kete shkronja");
        document.getElementById('cmimi1').focus();
        return false;
        
    }
 
    //cmimi2
     if(isNaN(v_cmimi2)) {
        alert("Cmimi-2 nuk duhet te kete shkronja");
        document.getElementById('cmimi2').focus();
        return false;
        
    }  
    
    //pershkrimi
    if(v_pershkrimi == null || v_pershkrimi == '' || v_pershkrimi ==' ') {
        alert("Pershkrimi nuk duhet te jene i zbrazet");
        document.getElementById('pershkrimi').focus();
        return false;
        
    }
 
     if(v_pershkrimi.length <3 ) {
        alert("Pershkrimi duhet te kete me shume se 3 karaktere");
         document.getElementById('pershkrimi').focus();
        return false;
    }
    
    if(!isNaN(v_pershkrimi)) {
        alert("Pershkrimi nuk duhet te permbaj numra");
        document.getElementById('pershkrimi').focus();
        return false;
    };
}

function kategoria_val(){
    var v_k_name = document.getElementById('k_name').value;
    
    if(v_k_name == null || v_k_name == '' || v_k_name ==' ') {
        alert("Emri i kategorise nuk duhet te jene i zbrazet");
        document.getElementById('k_name').focus();
        return false;
        
    }
 
     if(v_k_name.length <3 ) {
        alert("Emri i kategorise duhet te kete me shume se 3 karaktere");
         document.getElementById('k_name').focus();
        return false;
    }
    
    if(!isNaN(v_k_name)) {
        alert("Emri i kategorise nuk duhet te permbaj numra");
        document.getElementById('k_name').focus();
        return false;
    };
}

function foto_val(){

    var v_f_titulli = document.getElementById('f_titulli').value;
 //titulli_foto
    if(v_f_titulli == null || v_f_titulli == '' || v_f_titulli ==' ') {
        alert("Titulli nuk duhet te jene i zbrazet");
        document.getElementById('f_titulli').focus();
        return false;
        
    }
 
     if(v_f_titulli.length <3 ) {
        alert("Titulli duhet te kete me shume se 3 karaktere");
         document.getElementById('f_titulli').focus();
        return false;
    }
    
    if(!isNaN(v_f_titulli)) {
        alert("Titulli nuk duhet te permbaj numra");
        document.getElementById('f_titulli').focus();
        return false;
    };
    
   
    
}


function lajmet_val(){
    
    var titulli_l = document.getElementById('titulli').value;
    var pershkrimi_l = document.getElementById('pershkrimi').value;
   

    
    
     //titulli_l
    if(titulli_l == null || titulli_l == '' || titulli_l ==' ') {
        alert("Titulli nuk duhet te jene i zbrazet");
        document.getElementById('titulli').focus();
        return false;
        
    }
 
     if(titulli_l.length <3 ) {
        alert("Titulli duhet te kete me shume se 3 karaktere");
         document.getElementById('titulli').focus();
        return false;
    }
    
    if(!isNaN(titulli_l)) {
        alert("Emri nuk duhet te permbaj numra");
        document.getElementById('titulli').focus();
        return false;
    }
    
   //pershkrimi
    if(pershkrimi_l == null || pershkrimi_l == '' || pershkrimi_l ==' ') {
        alert("Pershkrimi nuk duhet te jene i zbrazet");
        document.getElementById('pershkrimi').focus();
        return false;
        
    }
 
     if(pershkrimi_l.length <3 ) {
        alert("Pershkrimi duhet te kete me shume se 3 karaktere");
         document.getElementById('pershkrimi').focus();
        return false;
    }
    
    if(!isNaN(pershkrimi_l)) {
        alert("Pershkrimi nuk duhet te permbaj numra");
        document.getElementById('pershkrimi').focus();
        return false;
    };
    
   
    
}
 
function staf_val(){
    
    var v_firstname = document.getElementById('name').value;
    var v_lastname = document.getElementById('surname').value;
    var v_dataLindjes = document.getElementById('data_linjdes').value;
    var v_profesioni = document.getElementById('profesioni').value;
    var v_pershkrimi = document.getElementById('pershkrimi').value;
    var username = document.getElementById('username').value;
    var fjalkalimi = document.getElementById('inputFjalekalimi').value;
    
    
    //username
    if(username == null || username == '' || username ==' ') {
        alert("Username nuk duhet te jene i zbrazet");
        document.getElementById('username').focus();
        return false;
        
    }
    //fjalkalimi
    if(fjalkalimi == null || fjalkalimi == '' || fjalkalimi ==' ') {
        alert("Fjalkalimi nuk duhet te jene i zbrazet");
        document.getElementById('inputFjalekalimi').focus();
        return false;
        
    }
     //Emri
    if(v_firstname == null || v_firstname == '' || v_firstname ==' ') {
        alert("Emri nuk duhet te jene i zbrazet");
        document.getElementById('name').focus();
        return false;
        
    }
 
     if(v_firstname.length <3 ) {
        alert("Emri duhet te kete me shume se 3 karaktere");
         document.getElementById('name').focus();
        return false;
    }
    
    if(!isNaN(v_firstname)) {
        alert("Emri nuk duhet te permbaj numra");
        document.getElementById('name').focus();
        return false;
    }
    
    //Mbiemri
     if(v_lastname == null || v_lastname == '' || v_lastname ==' ') {
        alert("Mbiemri nuk duhet te jene i zbrazet");
        document.getElementById('surname').focus();
        return false;
        
    }
 
     if(v_lastname.length <3 ) {
        alert("Mbiemri  duhet te kete me shume se 3 karaktere");
         document.getElementById('surname').focus();
        return false;
    }
    
    if(!isNaN(v_lastname)) {
        alert("Mbiemri nuk duhet te permbaj numra");
        document.getElementById('surname').focus();
        return false;
    }
    //DataLindjes
     if(v_dataLindjes == null || v_dataLindjes == '' || v_dataLindjes ==' ') {
        alert("Dite Lindja nuk duhet te jene i zbrazet");
        document.getElementById('data_lindjes').focus();
        return false;
        
    }
 
     
    
    //profesioni
     if(v_profesioni == null || v_profesioni == '' || v_profesioni ==' ') {
        alert("Profesioni nuk duhet te jene i zbrazet");
        document.getElementById('profesioni').focus();
        return false;
        
    }
 
     if(v_profesioni.length <2 ) {
        alert("Profesioni  duhet te kete me shume se 3 karaktere");
         document.getElementById('profesioni').focus();
        return false;
    }
    
    if(!isNaN(v_profesioni)) {
        alert("Profesioni nuk duhet te permbaj numra");
        document.getElementById('profesioni').focus();
        return false;
    }
    
    
    
    // Pershkrim
    
    if(v_pershkrimi == null || v_pershkrimi == '' || v_pershkrimi ==' ') {
        alert("Pershkrimi nuk duhet te jene i zbrazet");
        document.getElementById('pershkrimi').focus();
        return false;
        
    }
 
     if(v_pershkrimi.length <3 ) {
        alert("Pershkrimi  duhet te kete me shume se 3 karaktere");
         document.getElementById('pershkrimi').focus();
        return false;
    };
    
     if(!isNaN(v_pershkrimi)) {
        alert("Pershkrimi nuk duhet te permbaj numra");
        document.getElementById('pershkrimi').focus();
        return false;
    }
    
   
    
}
 
function per_val(){
    
    var firstname = document.getElementById('inputEmri').value;
    var lastname = document.getElementById('inputMbiemri').value;
    var username = document.getElementById('inputPerdoruesi').value;
    var password = document.getElementById('inputFjalekalimi').value;
    
    
    
     //Emri
    if(firstname == null || firstname == '' || firstname ==' ') {
        alert("Emri nuk duhet te jene i zbrazet");
        document.getElementById('inputEmri').focus();
        return false;
        
    }
 
     if(firstname.length <3 ) {
        alert("Emri duhet te kete me shume se 3 karaktere");
         document.getElementById('inputEmri').focus();
        return false;
    }
    
    if(!isNaN(firstname)) {
        alert("Emri nuk duhet te permbaj numra");
        document.getElementById('inputEmri').focus();
        return false;
    }
    //Mbiemri
     if(lastname == null || lastname == '' || lastname ==' ') {
        alert("Mbiemri nuk duhet te jene i zbrazet");
        document.getElementById('inputMbiemri').focus();
        return false;
        
    }
 
     if(lastname.length <3 ) {
        alert("Mbiemri  duhet te kete me shume se 3 karaktere");
         document.getElementById('inputMbiemri').focus();
        return false;
    }
    
    if(!isNaN(lastname)) {
        alert("Mbiemri nuk duhet te permbaj numra");
        document.getElementById('inputMbiemri').focus();
        return false;
    }
    
    //Username
     if(username == null || username == '' || username ==' ') {
        alert("Perdoruesi nuk duhet te jene i zbrazet");
        document.getElementById('inputPerdoruesi').focus();
        return false;
        
    }
 
     if(username.length <2 ) {
        alert("Perdoruesi  duhet te kete me shume se 2 karaktere");
         document.getElementById('inputPerdoruesi').focus();
        return false;
    }
    
    
    // PaSsword
    
    if(password == null || password == '' || password ==' ') {
        alert("Password nuk duhet te jene i zbrazet");
        document.getElementById('inputFjalekalimi').focus();
        return false;
        
    }
 
     if(password.length <6 ) {
        alert("Password  duhet te kete me shume se 6 karaktere");
         document.getElementById('inputFjalekalimi').focus();
        return false;
    };
    
   
    
}
function semundjet_val(){
    
    var s_name = document.getElementById('s_name').value;
    
    if(s_name == null || s_name == '' || s_name ==' ') {
        alert("Emri i semundjes nuk duhet te jene i zbrazet");
        document.getElementById('s_name').focus();
        return false;
        
    }
 
     if(s_name.length <3 ) {
        alert("Emri i semundjes duhet te kete me shume se 3 karaktere");
         document.getElementById('s_name').focus();
        return false;
    }
    
    if(!isNaN(s_name)) {
        alert("Emri i semundjes nuk duhet te permbaj numra");
        document.getElementById('s_name').focus();
        return false;
    };

}
function koha_val(){
    
    
  var inputOra = document.getElementById('inputOra').value;
    var inputMin = document.getElementById('inputMin').value;
    
    if(inputOra == null || inputOra == '' || inputOra == ' '){
        alert("Ora nuk mund te jete e zbrazet");
        document.getElementById('inputOra').focus();
        return false; 
    }
    if(inputOra.length != 2) {
        alert("Ora duhet te kete 2 karaktere");
         document.getElementById('inputOra').focus();
        return false;
    }  
     
    
    if(inputMin == null || inputMin == '' || inputMin == ' '){
        alert("Minutat nuk mund te jete e zbrazet");
        document.getElementById('inputMin').focus();
        return false; 
    }
    if(inputMin.length != 2) {
        alert("Minutat duhet te kete 2 karaktere");
         document.getElementById('inputMin').focus();
        return false;
    } 
}