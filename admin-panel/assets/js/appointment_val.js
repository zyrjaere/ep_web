function app_val(){
    
    var a_name = document.getElementById('a_ter_em_n').value;
    var a_email = document.getElementById('a_ter_email_n').value;
    var a_tel = document.getElementById('a_ter_tel_n').value;
    var a_date = document.getElementById('a_ter_data_n').value;
    var a_time = document.getElementById('a_ter_time_n').value;

  
    
     //Emri
    if(a_name == null || a_name == '' || a_name ==' ') {
        alert("Emri dhe Mbiemri nuk duhet te jene i zbrazet");
        document.getElementById('a_ter_em_n').focus();
        return false;
        
    }
 
     if(a_name.length <3 ) {
        alert("Emri dhe Mbiemri  duhet te kete me shume se 3 karaktere");
         document.getElementById('a_ter_em_n').focus();
        return false;
    }
    
    if(!isNaN(a_name)) {
        alert("Emri nuk duhet te permbaj numra");
        document.getElementById('a_ter_em_n').focus();
        return false;
    }
        
    
    //Telefoni
    if(a_tel == null || a_tel == '' || a_tel ==' ') {
        alert("Telefoni nuk duhet te jete i zbrazet");
        document.getElementById('a_ter_tel_n').focus();
        return false;
    }
     var count = 0;
     for (var i = 0; i < a_tel.length; i++){ 
        if (a_tel.charCodeAt(i) >= 65 || a_tel.charCodeAt(i) >= 90){
               count +=1;         
            }
         }
    if(count != 0 ){
        alert('Nr.Telefonit nuk duhet te permbaj numra');
        document.getElementById('a_ter_tel_n').focus();
        return false;
    }
    
    
    // Data
    

    if(a_date == null ||a_date == '' || a_date ==' ') {
        alert("Ju lutem caktoni Daten");
        document.getElementById('a_ter_data_n').focus();
        return false;
    }
 
    //Koha e terminit
    
    if(a_time == null ||a_time == '' || a_time ==' ' || a_time == "Koha e terminit HH:MM") {
        alert("Ju lutem caktoni Kohen e Terminit");
        document.getElementById('a_ter_time_n').focus();
        return false;
    };    
    //Statusi
    
    if(a_time == null ||a_time == '' || a_time ==' ' || a_time == "none") {
        alert("Ju lutem caktoni Kohen e Terminit");
        document.getElementById('a_ter_time_n').focus();
        return false;
    }
    
    
    
}