<?php
include '../../include/db.php';

// remove all session variables
session_unset(); 

// destroy the session 
session_destroy(); 


header("location:../../include/main-login.php");
?>