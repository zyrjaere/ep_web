<?php 
    include '../../include/db.php' ;
 
if(!($_SESSION['logged_in']))  {

     header("location:../../include/main-login.php");

}else{ 
    
    if($_SESSION['roli'] == '1' || $_SESSION['roli'] == '2'){
        
        if(isset($_GET['t_k_id'])){
            $id_termini = mysqli_real_escape_string($dbc,$_GET['t_k_id']);
            
            $query = "update terminet set `id_termin_statusi` = 1 where `id_terminet` = $id_termini";
            $konfirmo_terminin =  mysqli_query($dbc, $query) or die("Query failed! - " . mysqli_error($dbc));
            if(!($konfirmo_terminin)){
                mysqli_error($dbc);
            }
            
        }
             
        if(isset($_GET['t_d_id'])){
            $id_termini = mysqli_real_escape_string($dbc,$_GET['t_d_id']);
            
            $query = "DELETE FROM `terminet` WHERE `terminet`.`id_terminet` =$id_termini";
            $delete_terminin =  mysqli_query($dbc, $query) or die("Query failed! - " . mysqli_error($dbc));
            if(!($delete_terminin)){
                mysqli_error($dbc);
            }
            
        }
        
        if(isset($_GET['t_k_id'])){
            $id_termini = mysqli_real_escape_string($dbc,$_GET['t_k_id']);
            $query = "PDATE `terminet` SET `id_termin_statusi` = '1' WHERE `terminet`.`id_terminet` =$id_termini";
            $konfirmo_terminin =  mysqli_query($dbc, $query) or die("Query failed! - " . mysqli_error($dbc));
            if(!($konfirmo_terminin)){
                mysqli_error($dbc);
            }
            
        }
        
        if(isset($_GET['v_name'])){
            $sch_name = mysqli_real_escape_string($dbc,$_GET['v_name']);
            $today = date('Y-m-d');
             $query = "select t.id_terminet,t.emri_mbiemri,t.email,t.tel,kt.koha,t.data,s.emri,s.mbiemri,t.id_termin_statusi,t.id_prezent,t.koment from terminet t 
                 iNNER join staf_terminet st on st.id_staf_terminet = t.id_doktor_terminet INNER join staf s on s.id_staf = st.id_staf 
                 inner join koha_termineve kt on kt.id_koha_termineve=st.id_koha_terminet where LOWER(t.emri_mbiemri) like lower('%$sch_name%') and t.data >= '$today' 
                 order by t.data ,kt.koha asc";
             
             
            $ter_by_doc = mysqli_query($dbc, $query);
                        
            while($rows = mysqli_fetch_assoc($ter_by_doc)){
                 $id_termini = $rows ['id_terminet'];
                 $a_name = $rows ['emri_mbiemri'];
                 $a_email = $rows ['email'];
                 $a_tel = $rows ['tel'];
                 $a_koha = $rows ['koha'];
                 
                 $a_data = $rows ['data'];
                 $convert_date = strtotime($a_data);
                 $a_data = date('d-m-Y',$convert_date);
                 
                 $a_dok_emri = $rows ['emri'];
                 $a_dok_mbiemri = $rows ['mbiemri'];
                 $dr_fn = substr($a_dok_emri,0,1) . ".$a_dok_mbiemri";
                 $a_status = $rows ['id_termin_statusi'];
                 $a_prezent = $rows ['id_prezent'];
                 $a_koment = $rows ['koment'];
                 ?>
                <tr>
                <td><?php echo $a_name;?></td>
                <td><?php if($a_email == null){echo "-"; }else {echo $a_email;} ?></td>
                <td><?php echo $a_tel;?></td>
                <td><?php echo $a_koha;?></td>
                <td><?php echo $a_data;?></td>
                <td><?php echo $dr_fn;?></td>
                <td><a style='cursor:pointer'  id="<?php echo $id_termini ?>" onclick="show_terminet_by_id(this.id);">Ndrysho</a> </td>   
                
                <?php  if($a_status == '1' ){
                    echo "<td style='background-color: #00FF00;'>"
                    . "<a style='color:#111;cursor:pointer' id='$id_termini' class='$a_name $a_koha $a_data'  onclick='return konfirmo_terminet_by_id(this,this.id);'> Konfirmo </a></td> ";
                }               
                if($a_status == '2' ){
                  echo" <td style='background-color: #FF0000;'>"
                    . "<a style='color:#111;cursor:pointer' id='$id_termini' class='$a_name $a_koha $a_data'  onclick='return konfirmo_terminet_by_id(this,this.id);'> Konfirmo </a></td>";  
                }
                ?>
               <td> <?php echo "<a style='cursor:pointer' class='$a_name $a_koha $a_data' id='$id_termini' onclick='return delete_terminet_by_id(this,this.id);'>Largo</a>" ?></td>
               
               <td><form class="callus" style="text-align: center;"> 
                <?php if($a_prezent == 1){ ?> 
               <input type="checkbox" name="prezenca" onclick="ch_prezenca(this.id,this.checked);" checked id="<?php echo $id_termini;?>">
                   <?php }else{ ?> 
               <input type="checkbox" name="prezenca" onclick="ch_prezenca(this.id,this.checked);" id="<?php echo $id_termini;?>">  <?php }?>
                    </form></td>
                <td><form class="callus" onsubmit="return false;">
                    <input id="<?php echo $id_termini;?>" onkeyup="t_koment(this.id,this.value)"  type="text" maxlength="256" value="<?php echo $a_koment; ?>" >
                </form></td>  
                
                </tr>


               <?php 
                 
            }
            
        }
        
        if(isset($_GET['g_dta'])){
          $id_doc = mysqli_real_escape_string($dbc,$_GET['g_dta']);
          $today = date('Y-m-d');
          $query = "Select t.id_terminet,t.emri_mbiemri,t.tel,kt.koha,t.id_prezent,t.koment  from terminet t 
                                  INNER join staf_terminet st on st.id_staf_terminet = t.id_doktor_terminet  INNER join  staf s on  s.id_staf = st.id_staf 
                                  INNER join koha_termineve kt on kt.id_koha_termineve=st.id_koha_terminet where t.data = '$today' and st.id_staf = $id_doc  and t.id_termin_statusi =1 order by kt.koha asc";
          
                            $select_terminet_aktive = mysqli_query($dbc, $query);

                            while($row = mysqli_fetch_assoc($select_terminet_aktive)){
                                $id_termini = $row['id_terminet'];
                                $t_emri_mbiemri = $row['emri_mbiemri'];
                                $t_tel = $row['tel'];
                                $t_koha = $row['koha'];
                                $t_prezent = $row['id_prezent'];
                                $t_komenti = $row['koment'];
                        ?>
                    
                        <tr> 
                        <td><?php echo $t_emri_mbiemri ?></td>
                        <td><?php echo $t_tel ?></td>
                        <td><?php echo $t_koha ?></td>
                        <td> <form class="callus" style="text-align: center;"> <?php if($t_prezent == 1){ ?> <input type="checkbox" name="prezenca" onclick="ch_prezenca(this.id,this.checked);" checked id="<?php echo $id_termini;?>"><?php }else{ ?> <input type="checkbox" name="prezenca" onclick="ch_prezenca(this.id,this.checked);" id="<?php echo $id_termini;?>">  <?php }?></form></td>
                        <td>  <form class="callus" onsubmit="return false;"> <input id="<?php echo $id_termini;?>" onkeyup="t_koment(this.id,this.value)"  type="text" maxlength="256" value="<?php echo $t_komenti; ?>" ></form></td>
                        </tr>
        <?php
                    }//while
        }//if isset
        
        if(isset($_GET['g_dtp'])){
          $id_doc = mysqli_real_escape_string($dbc,$_GET['g_dtp']);
          $today = date('Y-m-d');
          $query = "Select t.id_terminet,t.emri_mbiemri,t.tel,kt.koha,t.data,t.id_termin_statusi,t.id_prezent,t.koment  from terminet t 
                                  INNER join staf_terminet st on st.id_staf_terminet = t.id_doktor_terminet  INNER join  staf s on  s.id_staf = st.id_staf 
                                  INNER join koha_termineve kt on kt.id_koha_termineve=st.id_koha_terminet where t.data = '$today' and st.id_staf = $id_doc and t.id_termin_statusi=2 order by kt.koha asc";
        
                            $select_terminet_passive = mysqli_query($dbc, $query);

                            while($row = mysqli_fetch_assoc($select_terminet_passive)){
                                $id_termini = $row['id_terminet'];
                                $t_emri_mbiemri = $row['emri_mbiemri'];
                                $t_tel = $row['tel'];
                                $t_koha = $row['koha'];
                                $t_data = $row['data'];
                        ?>
                        <tr>
                        <td><?php echo $t_emri_mbiemri ?></td>
                        <td><?php echo $t_tel ?></td>
                        <td><?php echo $t_koha ?></td>
                        <td><?php echo $t_data ?></td>
                        <td><a style='color:#111;cursor:pointer' id='<?php echo $id_termini; ?>' class='<?php echo $t_emri_mbiemri; ?>'  onclick='return konfirmo_terminet_by_id(this,this.id);'> Konfirmo </a></td>
                        </tr>
        <?php
                        }//while
        }//if
        
       
    }else{
        header("refresh:3; url=../../index.php"); 
        
    }
}?>
           