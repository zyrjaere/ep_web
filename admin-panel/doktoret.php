<?php
include '../include/db.php';
    if(isset($_SESSION['logged_in']))  {
        include 'include/header.php'; 
    if($_SESSION['roli'] == '1'){  ?>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
             <div class="row">
                    <div class="col-md-12">
                        <div class="panel-body" id="butonishto"> 
                    <a href="doktoret-detail.php" class="btn btn-default"><i class="fa fa-list" aria-hidden="true"></i> &nbsp; Lista e stafit</a>
                    
                     </div>
                        <h1 class="page-head-line">Stafi</h1>
                    </div>
                    
                </div>
 <?php                
                 if(isset($_GET['delete'])){

    // if(isset($_SESSION['user_role'])){

    //     if($_SESSION['user_role'] == 'admin'){

            $the_staf_id = mysqli_real_escape_string($dbc, $_GET['delete']);

            $query = "DELETE FROM staf WHERE id_staf = {$the_staf_id} ";
            $reset_ai="ALTER TABLE staf AUTO_INCREMENT = 1";         
            $delete_query = mysqli_query($dbc, $query) or die("Query failed! - " . mysqli_error($dbc));
            $reset_query = mysqli_query($dbc, $reset_ai) or die("Query failed! - " . mysqli_error($dbc));

            if($delete_query && $reset_query)
		{
			//Shfaq nje mesazh qe te dhenat u rujten me sukses dhe ridrejto ne index.php
			
            
            header("location:doktoret-detail.php?msg=delete" );
			
			
		}
		else{
			
				header("location:doktoret-detail.php?msg=failed" );
			
		}
        }
		?>
            
                  <div class="row">
                    <div class="col-md-12">
                        
                    </div>
                </div>
                   

                        <div class="panel panel-default">
                        <div class="panel-heading">
                           Staf
                        </div>
                        
                        <div class="panel-body">
                        <div class="table-responsive">
                        
   
                           <!-- EDIT STAF-->
                             <?php if(isset($_GET['edit_stafi'])) { 
                            
                 $the_staf_id = mysqli_real_escape_string($dbc, $_GET['edit_stafi']);            
                            
                  $query = "SELECT * FROM staf s INNER JOIN user u ON s.id_staf=u.id_staf WHERE s.id_staf = {$the_staf_id}";

                    $select_users = mysqli_query($dbc, $query);
                   
                    while($row = mysqli_fetch_assoc($select_users)){

                    $staf_id = $row['id_staf'];
                    $emri = $row['emri'];
                    $mbiemri = $row['mbiemri'];
                    $data_lindjes = $row['data_lindjes'];
                    $pozita = $row['id_pozita'];
                    $profesioni=$row['profesioni'];    
                    $pershkrimi = $row['pershkrimi'];
                    $perdoruesi = $row['username'];
                    $fjalkalimi = $row['password'];
                    $foto = $row['foto'];        
                            
                            
                            
                            
                            ?>
                                    
                       <form class="callus" enctype="multipart/form-data" method="post" >
  <div class="form-group">
    <label for="emri">Emri</label>
    <input name="emri" type="name" class="form-control" id="name" placeholder="Emri" value="<?php echo $emri; ?>" />
  </div>
  <div class="form-group">
    <label for="mbiemri">Mbiemri</label>
    <input name="mbiemri" type="name" class="form-control" id="mbiemri" placeholder="Mbiemri" value="<?php echo $mbiemri; ?>" />
  </div>
                           <div class="form-group">
    <label for="mbiemri">Data e Lindjes</label><br>
    <input name="data_lindjes" type="text" class="btn btn-default" id="data_lindjes"  placeholder="Data Lindjes" value="<?php echo $data_lindjes; ?>" />
  </div> 
  <label >Pozita</label> <br>
  <select data-toggle="dropdown" class="btn btn-default dropdown-toggle" name="id_staf_pozita" id="">
           <?php 
      $query = "SELECT * FROM staf_pozita where id_staf_pozita = {$pozita} ";
    $select_user_role = mysqli_query($dbc, $query);
    while($rows = mysqli_fetch_assoc($select_user_role)){
        $id=$rows['id_staf_pozita'];
        $sfat_role = $rows['emri'];
        echo'
            <option value="'.$id.'">'.$sfat_role.'</option>
          '?>
  <?php  }; ?>
        </select>
  
   <div class="form-group">
    <label for="profesioni">Profesioni</label>
    <input type="name" name="profesioni" class="form-control" id="name" placeholder="Profesioni" value="<?php echo $profesioni; ?>"/>
  </div>
  <div class="form-group">
  <label for="pershkrimi">Pershkrimi </label>
   <input name="pershkrimi" type="text" class="form-control" placeholder="Pershkrimi" value="<?php echo $pershkrimi; ?>" />
   </div>
  <!-- EDIT USER AND PASSWORD -->
  <hr>
  <div class="form-group">
    <label for="exampleInputEmail1">Perdoruesi</label>
    <input type="text" name="username" class="form-control" id="inputPerdoruesi" placeholder="Perdoruesi"  value="<?php echo $perdoruesi; ?>"/>
  </div>
  <label>Roli</label>		
        <br>								  
        <select data-toggle="dropdown" class="btn btn-default dropdown-toggle" name="roli" id="roli">
           
            <?php 
    
                    $query = "SELECT * FROM roli ";

                    $select_users = mysqli_query($dbc, $query);
                   
                    while($row = mysqli_fetch_assoc($select_users)){
                        $id_roli = $row['id_roli'];
                        $roli_name = $row['roli'];
                        echo '<option value="'.$id_roli.'">'.$roli_name.'</option>';
                        
                    
            
                        }
            ?>
            
        </select>
        <br><br>
           <div class="form-group">
                      
    <button onclick="new_pw();return false;" id="btn_new_password" class="btn btn-default" >New Password</button>
    <button onclick="old_pw();return false;" id="btn_old_password" style="display: none" class="btn btn-default" >Old Password</button>
    <div class="form-group" id="new_password" style="display: none">
    <label for="fjalekalimi">Fjalekalimi</label>
    <input type="password" name="password" class="form-control" id="inputFjalekalimi" placeholder="Fjalekalimi" value="" />
  </div>
     </div>
  <hr>
  <!-- END -->
  
   <div class="col-md-12">
  <div class="form-group">
      <div class="col-md-6">
    <label for="foto">Foton</label>
    <div class="row">
    <img src="../<?php echo $foto; ?>" style="width:356px;height:306px;" alt="foto_staf">
      </div>
    </div>
      
       <div class="col-md-6">
    <label for="foto">Ndrysho Foton</label>
    <input type="file" name="img_staf_o" id="img_staf_o" />
    <p class="help-block">Fotot te jene te madhesis 354 x 306 !..</p>
      </div>
  </div>
  </div>
                    
                     <hr />
  <?php if(isset($_GET['edit_stafi'])) {  
                          ?>
                           <button class="btn btn-default" style="margin-top:10px;" formaction='include/edit_stafi.php?staf_id=<?php echo $the_staf_id; ?>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;  Ruaj Ndryshimet</button>
                         
                         
                        <?php } ?>
                         
</form>
 
                           
  
                           
      
<div class="btn-group">
											  
											  

					<?php  } }else{ ?>
                                      <form enctype="multipart/form-data" method="post" >
  <div class="form-group">
    <label for="emri">Emri</label>
    <input name="emri" type="name" class="form-control" id="name" placeholder="Emri" />
  </div>
  <div class="form-group">
    <label for="mbiemri">Mbiemri</label>
    <input name="mbiemri" type="name" class="form-control" id="surname" placeholder="Mbiemri" />
  </div>
                                          <div class="form-group">
    <label for="Data lindjes">Data e Lindjes</label><br>
    <input name="data_lindjes" type="text" class="btn btn-default" id="data_lindjes"  placeholder="Data Lindjes"/>
  </div>
  <label >Pozita</label> <br>
  <select data-toggle="dropdown" class="btn btn-default dropdown-toggle" name="id_staf_pozita" id="id_staf_pozita">
           <?php 
      $query = "SELECT * FROM staf_pozita";
    $select_user_role = mysqli_query($dbc, $query);
    while($rows = mysqli_fetch_assoc($select_user_role)){
        $id=$rows['id_staf_pozita'];
        $sfat_role = $rows['emri'];
        echo'
            <option value="'.$id.'">'.$sfat_role.'</option>
          '?>
  <?php  }; ?>
        </select>
  
   <div class="form-group">
    <label for="profesioni">Profesioni</label>
    <input type="name" name="profesioni" class="form-control" id="profesioni" placeholder="Profesioni" />
  </div>
  <div class="form-group">
  <label for="pershkrimi">Pershkrimi</label>
   <input id="pershkrimi" name="pershkrimi" type="text" class="form-control" placeholder="Pershkrimi" />
   </div>
  <div class="form-group">
    <label for="foto">Shto Foton</label>
    <input type="file" id="img_staf" name="img_staf" />
    <p class="help-block">Fotot te jene te madhesis 354 x 306 !..</p>
      <progress id="progressBar" value="0" max="100" style="width:235px;"></progress> <br>
      <h3 id="status"></h3>
  </div>
  <div class="form-group">
       <hr>               
    <button onclick="new_per();return false;" id="btn_new_perdorues" class="btn btn-default" >Shto perdorues</button>
    
    <div class="form-group" id="perdoruesi" style="display: none">
        
        <label for="username">Perdorusi</label>
        <input type="text" name="username"  class="form-control" id="username" placeholder="Perdoruesi" value=""/>
    <label for="fjalekalimi">Fjalekalimi</label>
    <input type="password" name="password"   class="form-control" id="inputFjalekalimi" placeholder="Fjalekalimi" value="" />
     <label>Roli</label>		
        <br>								  
        <select data-toggle="dropdown" class="btn btn-default dropdown-toggle" name="roli" id="roli">
           
            <?php 
    
                    $query = "SELECT * FROM roli";

                    $select_users = mysqli_query($dbc, $query);
                   
                    while($row = mysqli_fetch_assoc($select_users)){
                        $id_roli = $row['id_roli'];
                        $roli_name = $row['roli'];
                        echo '<option value="'.$id_roli.'">'.$roli_name.'</option>';
                        
                    
            
                        }
            ?>
            
        </select>
  </div>
    <button onclick="anulo_per();return false;" id="btn_anulo" style="display: none" class="btn btn-default" >Anulo</button>
    
     </div>
  <button onclick="anulo_per();return false;" id="btn_anulo" style="display: none" class="btn btn-default" >Anulo</button>

                           <hr />
  <button type="submit"  onclick="return uploadFile();" class="btn btn-default"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Shto stafin</button>
                 <!--onclick="return uploadFile();"-->
</form>
 
                           
  
                           
      
<div class="btn-group">
											  
											  

                                                                 <?php } ?>
                            
      
    
     <!-- EDIT STAF-->
    
    
                            </div>
                </div>
                            </div>
                            
                     
                    
 </div>
            
            </div>
    <script src="assets/js/analizat_val.js"></script>
    <script src="assets/js/progressBar.js"></script>
    <script src="assets/js/new_pw.js"></script>
            
   <?php require'include/footer.php'; 
}else{ echo "<h1>'Nuk keni autorizim per te vazhduar'</h1>";
          header("refresh:3; url=terminet.php");} 
}else{ header("location: ../index.php");} ?>
