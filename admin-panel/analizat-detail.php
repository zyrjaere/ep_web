<?php 
include '../include/db.php';
    if(isset($_SESSION['logged_in']))  {
        include 'include/header.php';  
        if($_SESSION['roli'] == '1'){
?>
 <div class="content-wrapper">
        <div class="container">
         <div class="panel-body" id="butonishto"> 
                    <a href="analizat.php" class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Shto Analizen </a>
                    
                     </div>
        <h1 class="page-head-line">Analizat</h1>
 <div class="panel panel-default" >
                        <div class="pull-right shkarkocss">
                           <form action="include/excel.php" method="post">
                               <button class="btn btn-default" type="submit" name="export_excel" value=""><i class="fa fa-file-excel-o " aria-hidden="true"></i>&nbsp;&nbsp;Shkarko</button></form> &nbsp;
                            
                            </div>
                        <div class="panel-heading">
                           Lista e analizave 
                          
                        </div>
                        
                        <div class="panel-body" >
                           <?php    if (isset($_GET["msg"]) && $_GET["msg"] == 'sukses') {
echo "<p class='bg-success' > Analiza u ndryshua me sukses! </p>";
    
      header("refresh:1; url=analizat-detail.php ");
}
      elseif(isset($_GET["msg"]) && $_GET["msg"] == 'delete') {
echo "<p class='bg-success' > Analiza u fshi me sukses! </p>";
    
      header("refresh:1; url=analizat-detail.php ");
}elseif(isset($_GET["msg"]) && $_GET["msg"] == 'failed') {
echo "<p class='bg-failed' > Nuk perfundoj me sukses - ka ndodhu nje gabim! </p>";
    
      header("refresh:1; url=analizat-detail.php ");
}
    
    ?>
    
                            <div class="table-responsive" id="print-analiza" >
                                <table class="table table-striped table-bordered table-hover" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kategoria Analizave</th>
                                            <th>Emri</th>
                                            <th>Cmimi1</th>
                                            <th>Cmimi2</th>
                                            <th>Pershkrimi</th>
                                            <th>-</th>
                                            <th>-</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php                                         
            $query = "SELECT * FROM analizat";

            $select_users = mysqli_query($dbc, $query);

            while($row = mysqli_fetch_assoc($select_users)){

            $analiza_id = $row['id_analiza'];
            $kategoria_analizave = $row['id_kategoria_analizave'];
            $emri = $row['emri'];
            $cmimi1 = $row['cmimi1'];
            $cmimi2 = $row['cmimi2'];
            $pershkrimi = $row['pershkrimi'];      

                echo '<tr>';
                echo '<td>'.$analiza_id.'</td>';
                
                $query = "SELECT * FROM kategori_analiza WHERE id_kategoria_analizave = $kategoria_analizave ";
            $select_user_role = mysqli_query($dbc, $query);
            while($rows = mysqli_fetch_assoc($select_user_role)){

                $kategoria = $rows['emri'];

            }
                
                echo '<td>'.$kategoria.'</td>';
                
                echo '<td>'.$emri.'</td>';
                echo '<td>'.$cmimi1.'</td>';
                echo '<td>'.$cmimi2.'</td>';
                

                echo '<td>'.$pershkrimi.'</td>';
                echo "<td><a href='analizat.php?edit_lab={$analiza_id}'>Ndrysho</a></td>";
                echo "<td><a onclick='return MyFunction();' href='analizat.php?delete={$analiza_id}'> Fshij</a></td>";
                 echo'</tr>';



            }; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
     </div>
</div>
<script src="../js/print.js" type="text/javascript"></script>
<?php require'include/footer.php';  
        }else{ echo "<h1>'Nuk keni autorizim per te vazhduar'</h1>";
        header("refresh:3; url=terminet.php");}}else{ header("location: ../index.php");} ?>