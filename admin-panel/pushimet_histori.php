
<?php 
    include '../include/db.php';
if(isset($_SESSION['logged_in']))  {
include 'include/header.php'; 
if($_SESSION['roli'] == '1' || $_SESSION['roli'] == '2' ){

?>

<div class="content-wrapper">
        <div class="container">
            <div id="desc_pushimi">
                
            </div>

            
                   <h1 class="page-head-line">Historia e Pushimeve</h1>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lista e  pushimeve.
                      
                        </div>
                        <div class="panel-body">
                            <?php    if (isset($_GET["msg"]) && $_GET["msg"] == 'sukses') {
echo "<p class='bg-success' > Pushimi u ndryshua me sukses! </p>";
    
      header("refresh:1; url=pushimet_histori.php ");
}
      elseif(isset($_GET["msg"]) && $_GET["msg"] == 'delete') {
echo "<p class='bg-success' > Pushimi u fshi me sukses! </p>";
    
      header("refresh:1; url=pushimet_histori.php ");
}elseif(isset($_GET["msg"]) && $_GET["msg"] == 'failed') {
echo "<p class='bg-failed' > Nuk perfundoj me sukses - ka ndodhu nje gabim! </p>";
    
      header("refresh:1; url=pushimet_histori.php");
}
    
    ?>
                            <div class="table-responsive">
                               <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Perdoruesi</th>
                                            <th> Vjetor</th>
                                            <th> Mjekesor</th>
                                            <th> Personal</th>
                                            <th> Pa Pages</th>
                                            <th> Vjetor i mbetur</th>
                                            <th>-</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php 
                                            $query ="SELECT p.id_staf,  p.v , p.m , p.p , p.pap , p.vm FROM pushimi p INNER JOIN staf s ON s.id_staf=p.id_staf";
                                            $select_histori = mysqli_query($dbc, $query);
                                            while($row = mysqli_fetch_assoc($select_histori)){
                                                
                                                $id_staf = $row['id_staf'];
                                                $vjetor = $row['v'];
                                                $mjekesor = $row['m'];
                                                $personal = $row['p'];
                                                $pa_pages = $row['pap'];
                                                $vjetor_mbetur = $row['vm'];
                                            
                                            ?>
                                            <?php 
                                            $query1 = "SELECT emri,mbiemri  FROM staf where id_staf = $id_staf";

                                           $select_users_name = mysqli_query($dbc, $query1);

                                            while($row = mysqli_fetch_assoc($select_users_name)){
                                            $user_name = $row['emri'];
                                            $user_surname = $row['mbiemri'];
                                            $user_fname =  $user_name." ".$user_surname;
                                                    
                        }
                                            echo '<td>'.$user_fname.'</td>';
                                            ?>
                                            
                                            <?php  echo '<td>'.$vjetor.'</td>'; ?>
                                            <?php  echo '<td>'.$mjekesor.'</td>'; ?>
                                            <?php  echo '<td>'.$personal.'</td>'; ?>
                                            <?php  echo '<td>'.$pa_pages.'</td>'; ?>
                                            <?php  echo '<td>'.$vjetor_mbetur.'</td>'; ?>
                                           <?php echo "<td><a href='pushimet_histori_edit.php?edit_pushimet={$id_staf}'>Ndrysho</a></td>"; ?>
                                            
                                        </tr>
                                            <?php }; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
</div>
</div>

<script src="assets/js/desc_p_popup.js" type="text/javascript"></script>
<script src="assets/js/print_pushimi.js" type="text/javascript"></script>

<?php require'include/footer.php';  
        }else{ echo "<h1>'Nuk keni autorizim per te vazhduar'</h1>";
          header("refresh:3; url=terminet.php");} 
}else{ header("location: ../index.php");} ?>
