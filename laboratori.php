<!doctype html>

   <?php include'include/db.php'; ?>

<html lang="en">

<?php require'include/head.php'; ?>


<body>

<!--Loader-->



<!--Top bar-->



<header id="main-navigation">
  <?php
    require'include/header.php';
    ?>
</header>

<!--Page header & Title-->
<section id="page_header">

<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Laboratori</h2>
         <div class="page_link"><a href="index">Ballina</a><span><i class="fa fa-long-arrow-right"></i><a href="laboratori"><font color="red">Laboratori</font></a></span></div>
      </div>
    </div>
  </div>
</div>  
        

</section>
<section id="service" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <div id="desc_analiza" >
              
                  </div>
      <h2 class="heading">Analizat</h2>
      <hr class="half_space">
          
          <div class="search col-md-12 half_space" >
              
           <!--  SEARCH KEYUP   -->
            <div class="col-md-7  half_space">
               <form class="callus" onsubmit="return false">
              <label> Kërko analizen  </label>
                 
            <input class="form-control " placeholder="Kerko Analizen" onkeyup="search_analiza_spec(this)" id="sch_analiza_a">                   
                                                
                </form>
            </div>
        
           <!--  SEARCH KATEGORIA          -->
        <div class="col-md-3 pull-right half_space" >
               <form class="callus" onsubmit="return false">
              <label> Analizat në baze të Sëmundjes </label>
                 
            <select class="form-control "  onchange="search_analiza_kat();" id="sch_analiza_k">
                    
                <option value="all" >Të gjitha Analizat</option>
                <?php 

          $query2 = 'SELECT id_ls,smundja FROM `llojet_smundjeve` order by smundja asc';
          
          $select_k_analiza = mysqli_query($dbc, $query2);

           while($row = mysqli_fetch_assoc( $select_k_analiza)){
            $ls_id = $row['id_ls'];
            $smundja = $row['smundja'];
           
                      echo  '<option value="'.$ls_id.'">' .$smundja. '</option>';
                        
                }    ?>
                    </select>
                </form>
                 
            
            
            </div>
                      
        </div>
          
          <div class="col-md-7 col-sm-7">
        <div class="row">
            
        </div>
          
      <div class="service_wrapper">
          
      <ul class="items" id="show_analizat">
              
<!--          <li>
              <a href="#" class="expanded">Të gjitha Analizat</a>
          <ul class="sub-items bg_grey" style="display:block;">
              
              <form  method="get" id="qek-box-form" action="#"  >
        <?php 
            $query1= 'SELECT a.id_analiza,ka.id_kategoria_analizave, ka.emri kategoria, a.emri analiza,a.cmimi1 cmimi, a.pershkrimi
            FROM kategori_analiza ka INNER JOIN analizat a ON a.id_kategoria_analizave = ka.id_kategoria_analizave order by a.emri asc'; 
            
                $select_analizat = mysqli_query($dbc, $query1);

           while($row = mysqli_fetch_assoc( $select_analizat)){
            $a_id = $row['id_analiza'];
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria_a = $row['kategoria'];
            $analiza = $row['analiza'];
            $cmimi=$row['cmimi'];
            $pershkrimi = $row['pershkrimi'];
               ?>  
          
           <li>
              <div class="row">
                <div class="col-md-12 accordion-text">
                  <div id="hoveranalizat-div" class="analiza-chekbox">       
                      <input id="qek-box" type="checkbox" class="<?php echo $analiza; ?>" value="<?php echo $cmimi; ?>"><span id="<?php echo $a_id; ?>" style="cursor: pointer" onclick="show_desc_a(this.id)" class="analiza_n"><?php echo ' '.$analiza; ?></span><span class="cmimi-1" style="float:right"><?php echo $cmimi ?></span><span style="float:right">&nbsp;&#8364;</span><br><hr><br>
                  </div>
                </div>
              </div>
            </li>
        <?php }  ?>
              </form>
          </ul>
          </li>-->
           
       <?php 

          $query3 = 'select id_kategoria_analizave,emri from kategori_analiza order by emri asc';
          
          $select_k_analiza = mysqli_query($dbc, $query3);

           while($row = mysqli_fetch_assoc( $select_k_analiza)){
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria = $row['emri'];
           
          
          ?>
          <li>
          <a href="#"><?php echo $kategoria;?></a>
          <ul class="sub-items bg_grey">
              <form  method="get" id="qek-box-form"  action="#"  >
           
<?php $query= 'SELECT a.id_analiza,ka.id_kategoria_analizave, ka.emri kategoria, a.emri analiza,a.cmimi1 cmimi, a.pershkrimi
FROM kategori_analiza ka
INNER JOIN analizat a ON a.id_kategoria_analizave = ka.id_kategoria_analizave where ka.id_kategoria_analizave = '.$ka_id.' order by a.emri asc'; 
            
                $select_analizat = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_analizat)){
            $a_id = $row['id_analiza'];
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria_a = $row['kategoria'];
            $analiza = $row['analiza'];
            $cmimi=$row['cmimi'];
            $pershkrimi = $row['pershkrimi'];
               ?>  
          
           <li>
              <div class="row">
                <div class="col-md-12 accordion-text">
                  <div class="analiza-chekbox">       
                      <input id="qek-box" type="checkbox" class="<?php echo $analiza; ?>" value="<?php echo $cmimi; ?>">&nbsp;&nbsp;<span id="<?php echo $a_id; ?>" style="cursor: pointer" onclick="show_desc_a(this.id)" class="analiza_n"><?php echo ' '.$analiza; ?></span><span class="cmimi-1" style="float:right"><?php echo $cmimi ?></span><span style="float:right">&nbsp;&#8364;</span><br><hr><br>
                </div>
                  
                </div>
              </div>
            </li>
        <?php }  ?>
           </form>    
          </ul>
        </li>
        <?php }  ?>
        
      </ul>
         
          </div>
    </div>
                    <!--KALKULATORI-->
          
        <div class="col-md-5 col-sm-5" style="z-index:100" >
       
       
        <div class="price" id="price_calc" >
        
        <div class="kalkulatori"  id="kalcprice">
        
        <h2 id="kalkemri" >Kalkulatori</h2>
        <hr class="cal_hr">

      <div class="row">
      
      <div class="col-md-12">
     
    
      
       
          <div class="service_wrapper " id="items_analiza_list" style="background-color:#f0f5f7">
      <ul id="items">      
              
          </ul>
               
                </div> 
                <div class="totali">
                 
                   <i id="print_calc" class="fa fa-print" aria-hidden="true"> <input type="button"  formaction="../index.php" value="Print"  onclick="printDiv('kalcprice');"></i> 
                    
                 <h3 id="h3_total" style="float:right">Totali: 0.00 € </h3>
                 </div>
                  </div>
            
            </div>
            
            
        </div>
        </div>
      </div>
                        <!--KALKULATORI-->
      </div>
    </div>
  </div>

</section>
    
    




<!--Footer-->
<footer class="padding-top dark">
  <?php
    require'include/footer.php';
    ?>
</footer>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>

<!--<script src="js/popup.js" type="text/javascript"></script>-->
<script src="js/jquery-2.2.3.js" type="text/javascript"></script>
<script src="js/desc_a_popup.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.geolocation.edit.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/calc.js" type="text/javascript"></script>
<script src="js/functions.js" type="text/javascript"></script>
<script src="js/search_analiza.js" type="text/javascript"></script>
<script src="js/on_load_ex_script.js" type="text/javascript"></script>
<script src="js/print.js" type="text/javascript"></script>
</body>
</html>
